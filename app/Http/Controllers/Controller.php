<?php

namespace App\Http\Controllers;

use App\Normalizer;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function __construct(Request $request)
    {
        $this->request = $request;
        $routes = $request->route();

        if ($routes != null)
        {
            $elements = explode('@', $routes[1]['uses']);
            $this->controller = $elements[0];
            $this->action = $elements[1];
        }
    }

    public function normalize($klass, $options = [])
    {
        try {
            return new $klass($this->request, $this->action, $options);
        } catch (\Exception $e) {
            Log::error($message);

            return null;
        }
    }

    public function build_success_response($response, $code = 200)
    {
        $base_response = [ 'status' => 'success' ];

        $response = array_merge($base_response, $response);

        return response()->json($response, $code);
    }

    public function build_error_response($detail, $code = 422)
    {
        $response = [
            'status' => 'error',
            'detail' => $detail
        ];

        return response()->json($response, $code);
    }

    public function unknown_error_response($message = null)
    {
        $message = $message ? :trans('general.unhandle_errors');

        return $this->build_error_response($message, 500);
    }
}
