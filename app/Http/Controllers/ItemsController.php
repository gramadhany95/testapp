<?php

namespace App\Http\Controllers;
use DB;
use Log;
use App\Models\Checklist;
use Illuminate\Http\Request;
use App\Normalizer\ChecklistNormalizer;
use Illuminate\Support\Facades\Storage;

class ChecklistsController extends Controller
{
    public function index(Request $request)
    {
        $limit = min([$request->input('limit', 10), 50]);
        $offset = $request->input('offset', 0);
        $sorter = $request->input('order_by');
        $order = 'asc';

        if (!$sorter)
        {
            $sorter = 'checklist_id';
            $order = 'desc';
        }
        elseif (substr($sorter, 0, 1) == '-')
        {
            $order = 'desc';
            $sorter = ltrim($sorter, '-');
        }

        $query = Checklist::where($search_query);

        $ck_list = $query->orderBy($sorter, $order)->skip($offset)->take($limit)->get();
        $total = $query->count();

        return $this->build_success_response(['data' => $ck_list, 'total' => $total, 'limit' => $limit, 'offset' => $offset]);
    }

    public function create(Request $request)
    {
        $ck_list_normalizer = $this->normalize(ChecklistNormalizer::class);
        $ck_list_photo = $request->file('image_url');

        if ($ck_list_normalizer->isValid())
        {
            try{
                DB::beginTransaction();
                $ck_list = Checklist::create($ck_list_normalizer->permittedAttributes());

                DB::commit();
                if($ck_list)
                {
                    return $this->build_success_response(['data' => $ck_list]);
                }
            }
            catch(\Exception $e)
            {
                DB::rollback();
                Log::error($e);
                return $this->unknown_error_response();
            }
        }
        else
        {
            return $this->build_error_response($ck_list_normalizer->errors(), 422);
        }
    }

    public function update(Request $request, $id)
    {
        $ck_list = Checklist::find($id);

        if (!$ck_list) {
            return $this->build_error_response(trans('record_not_found'), 422);
        }

        $ck_list_normalizer = $this->normalize(ChecklistNormalizer::class);

        if ($ck_list_normalizer->isValid())
        {
            try
            {
                DB::beginTransaction();
                $ck_list->update($ck_list_normalizer->permittedAttributes());
                DB::commit();
                return $this->build_success_response(['data' => $ck_list]);
            }
            catch(\Exception $e)
            {
                DB::rollback();
                Log::error($e);
            }
        }
        else
        {
            return $this->build_error_response($ck_list_normalizer->errors(), 422);
        }
    }

    public function show($id)
    {
        $ck_list = Checklist::find($id);

        if($ck_list)
        {
            return $this->build_success_response(['data' => $ck_list]);
        }
        else
        {
            return $this->build_error_response(trans('general.record_not_found'), 422);
        }
    }

    public function destroy($id)
    {
        $ck_list = Checklist::find($id);

        if($ck_list->delete())
        {
            return $this->build_success_response(['data' => trans('general.operation_success')], 202);
        }
        else
        {
            return $this->build_error_response(trans('general.operation_failed'), 422);
        }
    }
}
