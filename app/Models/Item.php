<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'is_completed', 'completed_at', 'due', 'urgency', 
        'updated_by','updated_at','created_at','checklist_id'
    ];

    public function specialities()
    {
        return $this->belongsToMany('App\Models\Checklist', 'checklist_id')->withTimestamps();
    }

}
