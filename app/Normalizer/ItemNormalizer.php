<?php

namespace App\Normalizer;

class ChecklistNormalizer extends BaseNormalizer
{
    protected $attributes = [
        'description',
        'is_completed',
        'completed_at',
        'due',
        'urgency',
        'updated_by',
        'updated_at',
        'created_at',
        'assignee_id',
        'task_id'
    ];

    public function rules()
    {
        return [
            'description' => 'required|string',
            'is_completed' => 'required|boolean',
            'completed_at' => 'nullable',
            'due' => 'date|nullable',
            'urgency' => 'nullable',
            'updated_by' => 'nullable',
            'updated_at' => 'nullable',
            'created_at' => 'string',
            'assignee_id' => 'string|nullable',
            'task_id' => 'numeric|nullable'
        ];
    }

    public function permittedAttributes($use_default = true)
    {
        return parent::permittedAttributes($use_default);
    }
}
