<?php

namespace App\Normalizer;

use Validator;
use Illuminate\Http\Request;

class BaseNormalizer
{
    protected $request;
    protected $errors = null;
    protected $context = null;
    protected $permittedAttributes = null;
    protected $object = null;
    protected $options = [];
    protected $attributes = [];

    public function __construct(Request $request, $context = null, $options = [])
    {
        $this->request = $request;
        $this->context = $context;
        $this->options = $options;

        if (array_key_exists('object', $options))
        {
            $this->object = $options['object'];
        }
    }

    public function isValid()
    {
        if ($this->errors != null)
        {
            return false;
        }

        $validator = Validator::make($this->request->all(), $this->rules());

        if ($validator->fails())
        {
            $this->errors = $validator->errors();
            return false;
        }

        return true;
    }

    public function permittedAttributes($use_default = false)
    {
        return $this->getAttributes($use_default);
    }

    public function rules()
    {
        return [];
    }

    public function errors()
    {
        return $this->errors;
    }

    public function getAttribute($attr_name, $default = null)
    {
        return $this->request->input($attr_name, $default);
    }

    public function getAttributes($use_default = false)
    {
        if ($this->permittedAttributes)
        {
            return $this->permittedAttributes;
        }

        $context_attributes = $this->context . '_attributes';

        if (empty($this->{$context_attributes}) && $use_default)
        {
            $context_attributes = 'attributes';
        }

        if (property_exists($this, $context_attributes)){
            $this->permittedAttributes = $this->request->only($this->{$context_attributes});
            return $this->permittedAttributes;
        }

        return [];
    }

    public function transformAttributes($attributes)
    {
        if (!property_exists($this, 'attribute_names'))
        {
            return $attributes;
        }

        $result = [];

        foreach($attributes as $key => $value)
        {
            if (array_key_exists($key, $this->attribute_names))
            {
                $result[$this->attribute_names[$key]] = $value;
            }
            else
            {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}
