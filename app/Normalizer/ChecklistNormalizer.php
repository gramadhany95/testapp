<?php

namespace App\Normalizer;

class ChecklistNormalizer extends BaseNormalizer
{
    protected $attributes = [
        'checklist_domain',
        'checklist_id',
        'description',
        'is_completed',
        'completed_at',
        'updated_by',
        'updated_at',
        'created_at',
        'due',
        'urgency'
    ];

    public function rules()
    {
        return [
            'checklist_domain' => 'required|string',
            'checklist_id' => 'required|string',
            'description' => 'required|string',
            'is_completed' => 'boolean',
            'completed_at' => 'nullable',
            'updated_by' => 'string',
            'updated_at' => 'nullable',
            'created_at' => 'string',
            'due' => 'date|nullable',
            'urgency' => 'numeric'
        ];
    }

    public function permittedAttributes($use_default = true)
    {
        return parent::permittedAttributes($use_default);
    }
}
